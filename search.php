<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package foundry
 */

get_header(); ?>
</div>

	<div class="row">
		<div class="col s12 no-padding">
			<?php get_template_part( 'template-parts/nitc', 'featured-image' ); ?>
			<div class="clear"></div>
		</div>
	</div>
<div class="container">
	<div class="col s12" style="height: 150px;"></div>
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h3 class="center-align o-ticketing--name uppercase nitc-blue-text"><?php printf( esc_html__( 'Search Results for: %s', 'foundry' ), '<span style="text-decoration: underline;">' . get_search_query() . '</span>' ); ?></h3>
			</header><!-- .page-header -->
			<div class="col s12" style="height: 60px;"></div>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->
	<div class="col s12" style="height: 50px;"></div>
<?php

get_footer();
