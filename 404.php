<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package foundry
 */

get_header(); ?>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main o-position--relative" role="main">



			<section class="error-404 not-found">
				<div class="col s12 " style="height: 200px;"></div>
				<div class="o-position--absolute o-home--overlay ovlay-bottom z-index-back"></div>
				<div class="o-position--absolute o-home--overlay ovlay-middle z-index-back"></div>
				<div class="container">
				<header class="page-header center-align">
					<h1 class="page-title uppercase center-align"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'foundry' ); ?></h1>
				</header><!-- .page-header -->
					<div class="col s12 " style="height: 50px;"></div>
				<div class="page-content center-align">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'foundry' ); ?></p>
					<div class="col s12 " style="height: 50px;"></div>

					<?php get_search_form();?>

				</div><!-- .page-content -->
				</div>
			</section><!-- .error-404 -->
			<div class="col s12 " style="height: 100px;"></div>
		</main><!-- #main -->
	</div><!-- #primary -->
	<div class="container">
<?php
get_footer();
