<?php
/**
* Template Name: Home Page
 */

get_header();?>
    <div class="clear"></div>
    </div><!-- .container -->

    <div class="homepage-slider">
        <?php masterslider(2); ?>
    </div>

    <div class="container">
        <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb('<p id="breadcrumbs">','</p>');
        }?>
    </div>
    <div class="container"></div>
    <div class="row">
        <div id="primary" class="content-area col s12">
            <main id="main" class="site-main" role="main">

                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/foundry-home', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>

            </main><!-- #main -->
        </div><!-- #primary -->

        <?php
        //get_sidebar();?>
    </div>
<?php
get_footer();
