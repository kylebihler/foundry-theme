<?php
/**
 * foundry functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package foundry
 */

if ( ! function_exists( 'foundry_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function foundry_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on foundry, use a find and replace
	 * to change 'foundry' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'foundry', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'foundry' ),
        'footer' => esc_html__( 'Footer', 'foundry' ),
	) );

    // Register Navigation Menus
    function custom_navigation_menus() {

        $locations = array(
            'main_menu' => 'Main Menu',
            'secondary_menu' => 'Secondary Menu',
            'drawer_menu' => 'Drawer Menu',
        );
        register_nav_menus( $locations );

    }
    add_action( 'init', 'custom_navigation_menus' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'foundry_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'foundry_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function foundry_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'foundry_content_width', 640 );
}
add_action( 'after_setup_theme', 'foundry_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function foundry_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'foundry' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'foundry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'foundry_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function foundry_scripts() {
    // Add Material scripts and styles
    if( !is_admin()){

//        wp_deregister_script('jquery');
        wp_enqueue_script( 'material-jquery', 'http://code.jquery.com/jquery-2.1.3.min.js', array(), '1.0', false );

    }

//    Enqueue styles
    wp_enqueue_style( 'material-style', get_template_directory_uri() . '/css/materialize.min.css' );
    wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/css/slick.css' );
    wp_enqueue_style( 'animate-style', get_template_directory_uri() . '/css/animate.css' );
    wp_enqueue_style( 'foundry-style', get_stylesheet_uri() );

    wp_enqueue_style( 'MD_icons', '//fonts.googleapis.com/icon?family=Material+Icons' );

//    Enqueue scripts
    wp_enqueue_script( 'material-script', get_template_directory_uri() . '/js/materialize.min.js', array(), '1.0', false );
    wp_enqueue_script( 'material-custom', get_template_directory_uri() . '/js/material-custom-scripts.js', array(), '1.0', false );
    wp_enqueue_script( 'slick-custom', get_template_directory_uri() . '/js/slick.js', array(), '1.0', false );
    wp_enqueue_script( 'viewport', get_template_directory_uri() . '/js/viewport.js', array(), '1.0', false );
//    wp_enqueue_script( 'flowtype', get_template_directory_uri() . '/js/flowtype.js', array(), '1.0', false );
    wp_enqueue_script( 'foundry-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script( 'MorphSVGPlugin', get_template_directory_uri() . '/js/MorphSVGPlugin.min.js', array(), '1.19.0', true );

    //CDN enqueues
    wp_enqueue_script( 'TweenMax', '//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js', array(), '1.19.0', true );
    wp_enqueue_script( 'ScrollMagic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', array(), '2.0.5', true );
    wp_enqueue_script( 'Debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', array(), '2.0.5', true );
    wp_enqueue_script( 'FontAwesome', '//use.fontawesome.com/2a021e767a.js', array(), '4.7.0', true );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
    // Check our theme options selections, and load conditional styles
//    $themecolors = get_theme_mod( 'material_colors' );

    // Enqueue the selected color schema
//    wp_enqueue_style( 'foundry-colors', get_template_directory_uri() . '/css/'. $themecolors );

}
add_action( 'wp_enqueue_scripts', 'foundry_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Add theme options

function foundry_controls( $wp_customize ) {

// Add a section to customizer.php
    $wp_customize->add_section( 'foundry_options' ,
        array(
            'title'      => __( 'Foundry Options', 'foundry' ),
            'description' => 'The following theme options are available:',
            'priority'   => 30,
        )
    );

// Add setting
    $wp_customize->add_setting(
        'foundry_colors',
        array(
            'default' => 'foundry.css',
        )
    );

// Add control
    $wp_customize->add_control(
        'foundry_color_selector',
        array(
            'label' => 'Color Scheme',
            'section' => 'foundry_options',
            'settings' => 'foundry_colors',
            'type' => 'select',
            'choices' => array(
                'foundry.css' => 'foundry',
                'sss.css' => 'SSS',
            ),
        )
    );

// Section For Logos

$wp_customize->add_section(
    'foundry_new_section_logos' ,
    array(
        'title'      => __( 'Logos', 'foundry' ),
        'priority'   => 10,
        'capability'  => 'edit_theme_options',
        'description' => __('Change Logo options here.', 'foundry'),
    ) );

// Site Logo

$wp_customize->add_setting( 'header_logo',
    array(
        'default' => ''
    )
);

$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'header-logo',
        array(
            'label'      => __( 'Header Logo', 'foundry' ),
            'section'    => 'foundry_new_section_logos',
            'settings'   => 'header_logo',

        )
    )
);


// Footer Left Logo

$wp_customize->add_setting( 'footer_left_logo',
    array(
        'default' => ''
    )
);

$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'footer-logo-left',
        array(
            'label'      => __( 'Upload Left Footer Logo', 'foundry' ),
            'section'    => 'foundry_new_section_logos',
            'settings'   => 'footer_left_logo',

        )
    )
);

// Footer Right Logo

$wp_customize->add_setting( 'footer_right_logo',
    array(
        'default' => ''
    )
);

$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize,
        'footer-logo-right',
        array(
            'label'      => __( 'Upload Right Footer Logo', 'foundry' ),
            'section'    => 'foundry_new_section_logos',
            'settings'   => 'footer_right_logo',

        )
    )
);

    $wp_customize->add_section( 'social_settings', array(
        'title' => __( 'Social Media Icons', 'foundry' ),
        'priority' => 100,
    ));

    $social_sites = foundry_get_social_sites();
    $priority = 5;

    foreach( $social_sites as $social_site ) {

        $wp_customize->add_setting( "$social_site", array(
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'esc_url_raw',
        ));

        $wp_customize->add_control( $social_site, array(
            'label' => ucwords( __( "$social_site URL:", 'social_icon' ) ),
            'section' => 'social_settings',
            'type' => 'text',
            'priority' => $priority,
        ));

        $priority += 5;
    }

}
add_action( 'customize_register', 'foundry_controls' );

function foundry_get_social_sites() {

    // Store social site names in array
    $social_sites = array(
        'linkedin',
        'twitter',
        'facebook',
        'google-plus',
        'flickr',
        'pinterest',
        'youtube',
        'vimeo',
        'tumblr',
        'instagram',
        'email'
    );
    return $social_sites;
}
// Get user input from the Customizer and output the linked social media icons
function foundry_show_social_icons() {

    $social_sites = foundry_get_social_sites();

    // Any inputs that aren't empty are stored in $active_sites array
    foreach( $social_sites as $social_site ) {
        if ( strlen( get_theme_mod( $social_site ) ) > 0 ) {
            $active_sites[] = $social_site;
        }
    }

    // For each active social site, add it as a list item
    if ( !empty( $active_sites ) ) {
        echo "<ul class='social-media-icons'>";

        foreach ( $active_sites as $active_site ) { ?>

            <li>
                <a href="<?php echo get_theme_mod( $active_site ); ?>">
                    <?php if( $active_site == 'vimeo' ) { ?>
                        <i class="fa fa-<?php echo $active_site; ?>-square"></i> <?php
                    } else if( $active_site == 'email' ) { ?>
                        <i class="fa fa-envelope"></i> <?php
                    } else { ?>
                        <i class="fa fa-<?php echo $active_site; ?>"></i> <?php
                    } ?>
                </a>
            </li> <?php
        }
        echo "</ul>";
    }
}



Class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts {

    function widget($args, $instance) {

        extract( $args );

        $title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts') : $instance['title'], $instance, $this->id_base);

        if( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
            $number = 10;

        $r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true ) ) );
        if( $r->have_posts() ) :

            echo $before_widget;
            if( $title ) echo $before_title . $title . $after_title; ?>
            <ul class="collection rpwidget">
                <?php while( $r->have_posts() ) : $r->the_post(); ?>
                    <li class="collection-item avatar">
                        <?php echo get_the_post_thumbnail( $post_id, 'thumbnail', array( 'class' => 'alignleft circle' ) ); ?>
                        <span class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></span>
                        <p>by <?php the_author(); ?> on <?php echo get_the_date(); ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>

            <?php
            echo $after_widget;

            wp_reset_postdata();

        endif;
    }
}
function my_recent_widget_registration() {
    unregister_widget('WP_Widget_Recent_Posts');
    register_widget('My_Recent_Posts_Widget');
}
add_action('widgets_init', 'my_recent_widget_registration');

// Dress up the post navigation

add_filter( 'next_post_link' , 'my_nav_next' , 10, 4);
add_filter( 'previous_post_link' , 'my_nav_previous' , 10, 4);

function my_nav_next($output, $format, $link, $post ) {
    $text = ' previous post';
    $rel = 'prev';

    return sprintf('<a href="%1$s" rel="%3$s" rel="nofollow" class="waves-effect waves-light btn left"><span class="white-text"><i class="mdi-navigation-chevron-left left"></i>%2$s</span></a>' , get_permalink( $post ), $text, $rel );
}

function my_nav_previous($output, $format, $link, $post ) {
    $text = ' next post';
    $rel = 'next';

    return sprintf('<a href="%1$s" rel="%3$s" rel="nofollow" class="waves-effect waves-light btn right"><span class="white-text">%2$s<i class="mdi-navigation-chevron-right right"></i></span></a>' , get_permalink( $post ), $text, $rel );
}
// Custom comment functionality

add_filter('get_avatar','change_avatar_css');

function change_avatar_css($class) {
    $class = str_replace("class='avatar", "class='avatar circle left z-depth-1", $class) ;
    return $class;
}

add_filter('comment_reply_link', 'foundry_reply_link_class');


function foundry_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='waves-effect waves-light btn", $class);
    return $class;
}

function foundry_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                <?php
                /* translators: 1: date, 2: time */
                printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
            ?>
        </div>
        <?php printf( __( '<cite class="fn">%s</cite> <span class="says">wrote:</span>' ), get_comment_author_link() ); ?>
    </div>
    <?php if ( $comment->comment_approved == '0' ) : ?>
        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
        <br />
    <?php endif; ?>


    <?php comment_text(); ?>

    <div class="reply right">
        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
        <div class="clear"></div>
        </div>
    <?php endif; ?>
    <div class="divider"></div>
    <?php
}

if( ! class_exists( 'foundry_Walker_Desktop_Nav_Menu' ) ) :

    class foundry_Walker_Desktop_Nav_Menu extends Walker_Nav_Menu {

        private $curItem;

        /**
         * Starts the list before the elements are added.
         *
         * Adds classes to the unordered list sub-menus.
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         */
        function start_lvl( &$output, $depth = 0, $args = array() ) {

            // Depth-dependent classes.
            $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
            $display_depth = ( $depth + 1); // because it counts the first submenu as 0
            $classes = array(
                'sub-menu',
                ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
                ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
                'menu-depth-' . $display_depth
            );
            $class_names = implode( ' ', $classes );

            // Build HTML for output.
            $output .= "\n" . $indent . '<ul id="' . $this->curItem->post_name . '" class="' . $class_names . ' dropdown-content">' . "\n";
        }

        /**
         * Start the element output.
         *
         * Adds main/sub-classes to the list items and links.
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param object $item   Menu item data object.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         * @param int    $id     Current item ID.
         */
        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            global $wp_query;
            $this->curItem = $item;
            $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

            // Depth-dependent classes.
            $depth_classes = array(
                ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
                ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
                ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
                'menu-item-depth-' . $depth
            );

            $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

            // Passed classes.
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

            // Build HTML.
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

            // Link attributes.
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

            //double click fix KB
            if( in_array( 'menu-item-has-children', $item->classes ) ) {
                $attributes .= ' class="dropdown-button ';
            } else {
                $attributes .= ' class="';
            }

            $attributes .= 'menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

            if( in_array( 'menu-item-has-children', $item->classes ) ) {
                $attributes .= ' data-activates="' . $item->post_name . '" data-beloworigin="true" data-hover="true" data-inDuration="300" data-outDuration="225"';
            }
            //end double click fix

            // Build HTML output and pass through the proper filter.
            $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
                $args->before,
                $attributes,
                $args->link_before,
                apply_filters( 'the_title', $item->title, $item->ID ),
                $args->link_after,
                $args->after
            );
            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

        }
    }

endif;

if( ! class_exists( 'foundry_Walker_Drawer_Nav_Menu' ) ) :


    /**
     * Custom walker class.
     */
    class foundry_Walker_Drawer_Nav_Menu extends Walker_Nav_Menu {

        /**
         * Starts the list before the elements are added.
         *
         * Adds classes to the unordered list sub-menus.
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         */
        function start_lvl( &$output, $depth = 0, $args = array() ) {
            // Depth-dependent classes.
            $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
            $display_depth = ( $depth + 1); // because it counts the first submenu as 0
            $classes = array(
                'sub-menu',
                ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
                ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
                'menu-depth-' . $display_depth
            );
            $class_names = implode( ' ', $classes );

            // Build HTML for output.

            $output .= "\n" . $indent . '<ul class="' . $class_names . ' collapsible-body">' . "\n";

        }

        /**
         * Start the element output.
         *
         * Adds main/sub-classes to the list items and links.
         *
         * @param string $output Passed by reference. Used to append additional content.
         * @param object $item   Menu item data object.
         * @param int    $depth  Depth of menu item. Used for padding.
         * @param array  $args   An array of arguments. @see wp_nav_menu()
         * @param int    $id     Current item ID.
         */
        function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
            global $wp_query;
            $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

            // Depth-dependent classes.
            $depth_classes = array(
                ( $depth == 0 ? 'drawer-menu-item' : 'sub-menu-item' ),
                ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
                ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
                'menu-item-depth-' . $depth,
                'uppercase'
            );
            $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

            // Passed classes.
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

            // Build HTML.
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

            // Link attributes.
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
//            $attributes .= ' class="menu-link waves-effect ' .( $depth == 0 ? 'sub-menu-link collapsible-header ' : 'main-menu-link' ) . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';


            //double click fix KB
            if( in_array( 'menu-item-has-children', $item->classes ) ) {
                $attributes .= ' class="menu-link waves-effect collapsible-header ';
            } else {
                $attributes .= ' class="';
            }

            $attributes .= 'menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

            //end double click fix



            // Build HTML output and pass through the proper filter.
            $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
                $args->before,
                $attributes,
                $args->link_before,
                apply_filters( 'the_title', $item->title, $item->ID ),
                $args->link_after,
                $args->after
            );
            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        }
    }
endif;

add_action( 'init', 'my_add_excerpts_to_pages' );

function my_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt' );
}