<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foundry
 */

?>
		<div class="clear"></div>
		</div><!-- .container -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<!-- Upper Footer --->

		<div class="row">
			<div class="col s12 no-padding">
				<?php get_template_part( 'template-parts/foundry', 'upper-footer' ); ?>
				<div class="clear"></div>
			</div>
		</div>

		<!-- Lower Footer --->

		<div class="row">
			<div class="col s12 no-padding">
				<?php get_template_part( 'template-parts/foundry', 'lower-footer' ); ?>
				<div class="clear"></div>
			</div>
		</div>

		<div class="site-info container">

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
