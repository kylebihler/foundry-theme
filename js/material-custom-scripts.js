jQuery(document).ready(function ($) {

    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            hover: true, // Activate on hover
            belowOrigin: true, // Displays dropdown below the button
            alignment: 'right' // Displays dropdown with edge aligned to the left of button
        }
    );

    $('.more-artist--related').mouseenter(function(){
            $('.animated-btn' ,this).addClass( "display-btn animated fadeInUp");
    });
    $('.more-artist--related').mouseleave(function(){
        $('.animated-btn' ,this).removeClass( "display-btn animated fadeInUp");
        $('.animated-btn' ,this).addClass( "animated fadeInDown");
    });

    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            edge: 'left', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
    );
    Materialize.updateTextFields();

    // $('li.main-menu-item').addClass('waves-effect waves-light');

    $('.slider').slider({full_width: true, indicators:true, interval: 10000,});

    $('.carousel').carousel({shift: 20, dist: 0, indicators: true, padding: 10,});

    $('.modal-trigger').leanModal();
    
    $('.collapsible').collapsible();

    $('.parallax').parallax();

    // $('h1').flowtype({
    //     minimum   : 500,
    //     maximum   : 1200,
    //     minFont   : 12,
    //     maxFont   : 80,
    //     fontRatio : 5
    // });
    //

    $('.variable-width').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1000,
        draggable: true,
        infinite:false,
        arrows: false,

        centerPadding: '50px',
        lazyLoad: 'ondemand',
        respondTo: 'window' ,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3,
                }
            }
        ]

    });

});

console.log('\n %c Made at Foundry with lots of ♥ %c http://foundryideas.com 👌🏼', 'color: #000; background: #ccc; padding: 5px 20px;', ' background: #FF6C0C; padding:5px 10px; color:#fff;');


