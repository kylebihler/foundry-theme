<?php
/**
 * The template part for displaying the main navigation
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package foundry
 */

wp_nav_menu(
    array(
        'theme_location' => 'main_menu',
        'menu_id' => 'desktop-nav',
        'walker' => new foundry_Walker_Desktop_Nav_Menu(),
        'menu_class' => 'hide-on-med-and-down'

    )
);?>


