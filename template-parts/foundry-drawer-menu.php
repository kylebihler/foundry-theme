<?php
/**
 * The template part for displaying the main navigation
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package foundry
 */


 $logo = get_theme_mod('header_logo'); // Located in Customize

wp_nav_menu(
    array(
        'theme_location' => 'drawer_menu',
        'menu_class' => 'menu side-nav',
        'menu_id' => 'slide-out',
        'items_wrap' => '<div id="%1$s" class="%2$s"><div class="mobile-header"><img src="'.$logo.'"/></div><ul class="drawer-nav collapsible" data-collapsible="accordion">%3$s</ul></div><div class="clear"></div>',
        'walker' => new foundry_Walker_Drawer_Nav_Menu(),

    )
);
