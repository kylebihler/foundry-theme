<!-- Modal Structure -->
<div id="search1" class="modal modal-fixed-footer">
    <span class="close-dis right">
        <a href="#!" class=" modal-action modal-close">
            <i class="fa fa-times" aria-hidden="true"></i>
        </a>
    </span>

    <div class="row pad-modal">
        <h3 class="center-align uppercase ">Can't seem to find what you are looking for?</h3>
        <h6 class="center-align">Try searching the term</h6>
        <?php get_search_form(); ?>
    </div>
</div>