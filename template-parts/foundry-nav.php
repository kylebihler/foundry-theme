<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foundry
 */

?>

<div class="m-container nav navbar-fixed">
    <nav id="site-navigation" role="navigation" class="nav-background">
        <div class="nav-wrapper">
            <div class="row">
                <div class="col l3 hide-on-med-and-down">
                    <div class="header-logo">
                        <a href="<?php echo get_home_url();?>"><img class="responsive-img" src="<?php echo get_theme_mod('header_logo'); // Located in Customize ?>" alt="<?php bloginfo( 'name' ); ?>"></a>
                    </div>
                </div>
                <div class="col l7 m3 s2 center">
                    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
                    <?php get_template_part( 'template-parts/foundry', 'desk-menu' ); ?>
                </div>

                <div class="col m6 s10 center hide-on-large-only">
                    <div class="header-logo">
                        <a href="<?php echo get_home_url();?>"><img class="responsive-img left" src="<?php echo get_theme_mod('header_logo'); // Located in Customize ?>" alt="<?php bloginfo( 'name' ); ?>"></a>
                    </div>
                </div>
    <!--            <div class="right search-link col s3">-->
    <!--                <a class="modal-trigger" href="#search1"><i class="material-icons">search</i></a>-->
    <!--            </div>-->

                <div class="right col l2 m3 hide-on-small-only">
                    <?php get_template_part( 'template-parts/foundry', 'social-links' ); ?>
                </div>
            </div>
        </div>
    </nav><!-- #site-navigation -->
    <?php get_template_part( 'template-parts/foundry', 'drawer-menu' ); ?>
</div>

