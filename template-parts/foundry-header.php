<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foundry
 */

?>
<div>
    <?php get_template_part( 'template-parts/foundry', 'nav' ); ?>
</div>

<?php //get_template_part( 'template-parts/foundry', 'search' ); ?>

    <?php if (! is_front_page()) : ?>
    <div class="row">

        <div class="o-position--absolute subpage-logo hide-on-med-and-down">
            <a href="<?php echo get_home_url('/'); ?>">
                <img class="center" src="<?php echo get_theme_mod('header_logo'); // Located in Customize ?>" alt="<?php bloginfo( 'name' ); ?> Logo">
            </a>
        </div>
        <div class="o-position--absolute mobile-logo hide-on-large-only col s12 no-padding">
            <a href="<?php echo get_home_url('/'); ?>">
                <img class="center" src="<?php echo get_theme_mod('header_logo'); // Located in Customize ?>" alt="<?php bloginfo( 'name' ); ?> Logo">
            </a>
        </div>

    </div>
    <?php endif; ?>
</div>

