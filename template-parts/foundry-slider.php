<?php
/**
 * Created by PhpStorm.
 * User: Jaycob
 * Date: 10/25/16
 * Time: 11:28 AM
 */

$posts = get_posts(array(
    'post_type' => 'foundryslider',
    'orderby' => 'post__in',
    'posts_per_page' => -1,
    'order'            => 'ASC',
));


?>


<div class="slider">
    <ul class="slides">

        <?php foreach ($posts as $post) {

            $headline = get_post_meta($post->ID, '_ct_text_5817c88921baa');
            $headlineColor = get_post_meta($post->ID, '_ct_selectbox_5817ca616bd9e');


            $subheadline = get_post_meta($post->ID, '_ct_text_5817c8a040194');
            $subheadlineColor = get_post_meta($post->ID, '_ct_text_5817cbf232326');


            $img_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

            $btnlabel = get_post_meta($post->ID, '_ct_text_5817c8b76c2f9');
            $link = get_post_meta($post->ID, '_ct_text_5817c8c629d94');

           $textPosition = get_post_meta($post->ID, '_ct_radio_58338a7a4a9b5');

            if ($textPosition[0] == 'Center' ) {
                $class = 's12';
            } elseif ($textPosition[0] == 'Left' ){
                $class = 's12 l7';
            } elseif ($textPosition[0] == 'Right' ){
                $class = 's12 l7 offset-l5';
            }


         ?>



        <li>
            <img src="<?php echo $img_url;?>"> <!-- random image -->

            <div class="row o-position--absolute slider-overlay">
                <div class="col <?php echo $class; ?> o-position--relative valign-wrapper slider-layer">

                    <div class="caption center-align valign">
                        <h2 style="color:<?php echo $headlineColor[0]; ?>;margin-bottom: 5px;"><?php echo $headline[0]; ?></h2>
                        <h5 style="color:<?php echo $subheadlineColor[0]; ?>;text-transform: uppercase;font-weight: 300;font-size: 18px;"><?php echo $subheadline[0]; ?></h5>
                        <div style="width: 100%;height: 20px;"></div>
                        <?php if (!empty($link[0] && $btnlabel[0])){ ?>
                            <a href="<?php echo $link[0]; ?>">
                                <span class="yellow-btn nitc-btn waves-effect waves-light"><?php echo $btnlabel[0]; ?></span>
                            </a>
                       <?php } ?>


                        <div class="clear"></div>
                    </div>

                </div>
            </div>
        </li>
    <?php } ?>
    </ul>
</div>
