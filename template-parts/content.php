<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package foundry
 */


	$currentContent = get_post();// Displays page content of current page
	$pageTitle = $post->post_title;
	?>


	<div class="row">
		<div class="col s12 no-padding">
			<?php get_template_part( 'template-parts/nitc', 'featured-image' ); ?>
			<div class="clear"></div>
		</div>
	</div>

	<div class="container">
		<div class="row" style="margin-top:90px;">
			<div class="col s12">
				<div class="page-title"><h1 class="uppercase neon-green-text"><?php echo $pageTitle; ?></h1></div>
				<div style="width:100%;height:40px;"></div>
				<?php echo apply_filters( 'the_content', $currentContent->post_content ); ?>
				<div style="width:100%;height:80px;" class="clear"></div>
			</div>
		</div>
	</div>

	<div style="width:100%;height:60px;"></div>





